unit LinkedListsUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  PIntElement = ^TIntElement;

  TIntElement = record
    next: PIntElement;
    data: LongInt;
  end;

  function CreateIntElement(): PIntElement;
  procedure DestroyTIntElement(IntElement: PIntElement);

implementation

function CreateIntElement: PIntElement;
var
  NewPTIntElement: PIntElement;
begin
  new(NewPTIntElement);
  if NewPTIntElement = nil then
  begin
    Result := nil;
    Exit;
  end;

  NewPTIntElement^.next := nil;
  Result := NewPTIntElement;

end;

procedure DestroyTIntElement(IntElement: PIntElement);
begin

end;

end.

